package com.example.tedrssviewer;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

class XMLResponseHandler implements ResponseHandler<List<String>> {
	// ����� ���� �������� ���������� ����� rss �������, ��� ������� ������� �� ��������
	
	private static final String ITEM_TAG = "item";
	private static final String TITLE_TAG = "title";
	private static final String LINK_TAG = "enclosure";
	private static final String DESC_TAG = "description";
	
	private String mTitle, mLink, mDesc;
	private boolean mIsParsingTitle, mIsParsingLink, mIsParsingDesc;
	private final List<String> mResults = new ArrayList<String>();
	public final List<String> mResult = new ArrayList<String>();
	
	@Override
	public List<String> handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {
		try {

			// Create the Pull Parser
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser xpp = factory.newPullParser();

			// Set the Parser's input to be the XML document in the HTTP Response
			xpp.setInput(new InputStreamReader(response.getEntity()
					.getContent()));
			
			// Get the first Parser event and start iterating over the XML document 
			int eventType = xpp.getEventType();
			
			
			while (eventType != XmlPullParser.END_DOCUMENT) {

				if (eventType == XmlPullParser.START_TAG) {
					startTag(xpp);
				} else if (eventType == XmlPullParser.END_TAG) {					
					endTag(xpp.getName());					
				} else if (eventType == XmlPullParser.TEXT) {
					text(xpp.getText());
				}
				eventType = xpp.next();
			}
			return mResults;
		} catch (XmlPullParserException e) {
		}
		return null;
	}

	public void startTag(XmlPullParser parser) {
		String localName = parser.getName();
		if (localName.equals(TITLE_TAG)) {
			mIsParsingTitle = true;
		} else if (localName.equals(LINK_TAG)) {
			mLink = parser.getAttributeValue(null, "url");			
		} else if (localName.equals(DESC_TAG)) {
			mIsParsingDesc = true;
		}
		
	}

	public void text(String text) {
		if (mIsParsingTitle) {
			mTitle = text.trim();
//		} else if (mIsParsingLink) {
//			mLink = parser.getAttributeValue(null, "url");
		} else if (mIsParsingDesc) {
			mDesc = text.trim();
		}
	}
	

	public void endTag(String localName) {
		if (localName.equals(ITEM_TAG)) {
			mResults.add(mTitle);			
			mResult.add(mLink);
			mTitle = null;
			mLink = null;
			mDesc = null;						
		} else if (localName.equals(TITLE_TAG)) {
			mIsParsingTitle = false;
		} else if (localName.equals(DESC_TAG)) {
			mIsParsingDesc = false;
		}
	}
}
	
