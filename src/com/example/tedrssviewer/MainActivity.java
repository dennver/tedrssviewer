package com.example.tedrssviewer;            

import java.io.IOException;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
//import 

public class MainActivity extends ListActivity {

	public static final String LINK = "link";	
	private HttpGetTask mTask = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mTask = (HttpGetTask) new HttpGetTask().execute();
	}

	private class HttpGetTask extends AsyncTask<Void, Void, List<String>> {

		private static final String URL = "http://www.ted.com/talks/rss";

		AndroidHttpClient mClient = AndroidHttpClient.newInstance("");
		
		public List<String> mResult = null;

		@Override
		protected List<String> doInBackground(Void... params) {
			HttpGet request = null;
			try {
				request = new HttpGet(URL);				

			} catch (IllegalArgumentException e) {
				e.printStackTrace();				
	            return null;				
			}
			XMLResponseHandler responseHandler = new XMLResponseHandler();
			mResult = responseHandler.mResult;
			try {
				return mClient.execute(request, responseHandler);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(List<String> result) {			
			if (null != mClient) 
				mClient.close();
			if (null == result) {
				AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
				builder1.setMessage("Check your system settings and internet connection");
				builder1.setNegativeButton("Quit",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
//						dialog.cancel();						
						System.exit(0);
					}
				});

				AlertDialog alert11 = builder1.create();
				alert11.show();				
			}else {
				setListAdapter(new ArrayAdapter<String>(
						MainActivity.this,
						R.layout.activity_main, result));
			}			
		}
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {
		
		// Create implicity Intent to start the VideoViewer class
		Intent showItemIntent = new Intent(MainActivity.this,
				VideoViewerActivity.class);
		
		// Add an Extra representing the currently selected position
		// The name of the Extra is stored in INDEX

		showItemIntent.putExtra(LINK, (String)mTask.mResult.get(pos));

		startActivity(showItemIntent);
	}
}
