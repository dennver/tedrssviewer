package com.example.tedrssviewer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;


public class VideoViewerActivity extends Activity {
    private VideoView mVideoView = null;
    private ProgressDialog progressDialog = null;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video);
		
		// Get the Intent that started this Activity
		Intent intent = getIntent();
		String link = intent.getStringExtra(MainActivity.LINK);

		
		mVideoView = (VideoView) findViewById(R.id.videoView1);		
		progressDialog = new ProgressDialog(VideoViewerActivity.this);
		// set a title for the progress bar
		progressDialog.setTitle("Android TED Video Viewer");
		// set a message for the progress bar
		progressDialog.setMessage("Loading...");
		//set the progress bar not cancelable on users' touch
		progressDialog.setCancelable(false);
		// show the progress bar
		progressDialog.show();


		final MediaController mMediaController = new MediaController(VideoViewerActivity.this);
//		mMediaController.setEnabled(false);
		

		if (null != link) {
			try {
				mVideoView.setMediaController(mMediaController);
				mVideoView.setVideoURI(Uri.parse(link));

			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}

			mVideoView.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					// TODO Auto-generated method stub
					// close the progress bar and play the video
					progressDialog.dismiss();
					mMediaController.setEnabled(true);
				}
			});
			mVideoView.start();
		}
	}
	
    // Release resources
	protected void onPause() {
		if (mVideoView != null && mVideoView.isPlaying()) {
			mVideoView.stopPlayback();
			mVideoView = null;			
		}
		super.onPause();
	}
}